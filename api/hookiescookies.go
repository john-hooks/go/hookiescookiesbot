package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var token = os.Getenv("TOKEN")
var port = os.Getenv("PORT")
var url = os.Getenv("URL")
var chatID = os.Getenv("chatID")

type Request struct {
	Name    string `json:"name"`
	Email   string `json:"email"`
	Subject string `json:"subject"`
	Message string `json:"message"`
}

type Response struct {
	ChatID string `json:"chat_id"`
	Text   string `json:"text"`
}

func sendMessage(r Request) {
	log.Println("Got message, sending to Telegram")

	message := fmt.Sprintf("New message from the site. %s says %s: %s. Their email is %s", r.Name, r.Subject, r.Message, r.Email)
	response := Response{
		ChatID: chatID,
		Text:   message,
	}

	log.Println(response)

	botURL := "https://api.telegram.org/bot" + token + "/sendMessage"

	var body []byte

	body, err := json.Marshal(response)
	if err != nil {
		log.Println(err)
	}

	req, err := http.Post(botURL, "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Println(err)
	}

	defer req.Body.Close()

}

func Handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	// Set CORS headers for the main request.
	w.Header().Set("Access-Control-Allow-Origin", "*")

	log.Println("request received")

	if r.Method != "POST" {
		return
	}

	request := Request{}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal(body, &request)

	sendMessage(request)

	fmt.Fprint(w, "sent\n")
}
